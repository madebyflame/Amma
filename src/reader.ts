import * as fs from 'mz/fs';
import * as path from 'path';

import {FILINFO, FileOptions, Directory} from './types';

/**
 * @function reader
 * 
 * reads a directory recursively
 * 
 * @param directory String
 * @param options FileOptions
 * @return Promise<Directory>
 *
 * 
 */

export async function reader(directory: string, options: FileOptions = {}): Promise<Directory> {
  let read: Directory = Object.create(null); 
  return new Promise<Directory>(async function(resolve, reject) { 
    try {
      const stat  = await fs.stat(directory);
      const files = await fs.readdir(directory)
      read[directory] = stat;

      while (files.length) {
        const file = files.shift();
        if (options.filter && !options.filter(file)) 
          continue ;

        const fullPath = path.join(directory, file);
        const stat     = await fs.stat(fullPath);
        read[fullPath] = stat;

        if (stat.isDirectory()) {
          const search = await reader(fullPath, options);
          read = {...read, ...search};
        }
      }
    } catch(error) {
      reject(error);
    }
    resolve(read);
  });
}