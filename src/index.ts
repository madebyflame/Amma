import { EventEmitter } from 'events';
import * as fs from 'mz/fs';

import { Watcher as FSWatcher } from '../ext';

import {
  FILINFO, FileOptions, Watcher, Directory
} from './types';

export * from './reader';

/**
 * @class Amma
 * 
 * singleton file monitor that exposes an EventEmitter
 *  
 */

export default class Amma {
  private static _instance: Amma;
  public watch: Watcher;

  private _options: FileOptions = {
    recursive: true
  }
  // get: EventEmitter
  get emitter(): EventEmitter {
    return this.watch.expose();
  }

  /**
   * @method constructor
   * @param options FileOptions
   * @return void
   * 
   * create singleton instance
   */

  private constructor(options: FileOptions) {
    if(!Amma._instance){
      this.watch     = <Watcher>new FSWatcher();
      this._options  = options || this._options;
      Amma._instance = this;
    }
  }

  /**
   * @method getInstance
   * @param options? FileOptions
   * @return Amma
   * 
   * return singleton instance
   */

  static getInstance(options?: FileOptions): Amma {
    return this._instance || (this._instance = new this(options));
  }

  /**
   * @method add
   * @param directory string
   * @return void
   * 
   * add directory to watch
   */

  add(directory: string): void {
    this.watch.watchDirectory(directory, this._options);
  }

  /**
   * @method delete
   * @param options FileOptions
   * @return void
   * 
   * delete directory from watch
   */

  delete(directory: string): void {
    this.watch.close(directory);
  }

  /**
   * @method delete
   * @return void
   * 
   * clear watch tree
   */

  close(): void {
    this.watch.close();
  }

}