import * as fs from 'mz/fs';
import { EventEmitter } from 'events';
export type FILINFO = fs.Stats;

export interface Watcher {
  watchers: Watchee;
  watchDirectory(directory: string, options: FileOptions): void;
  close(directory?: string): void;
  expose(): EventEmitter;
}

export interface FileOptions {
  recursive?: boolean,
  filter?: (file: string) => boolean
}

export interface Watchee {
  [key:string]: Watcher
}

export interface Directory {
  [key:string]: FILINFO
}