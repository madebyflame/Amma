import Amma = require("../src/reader");
import * as fs from 'mz/fs';
import * as chai from "chai";
const expect = chai.expect;

describe("Reader", function() {
  describe('#reader', function() {
    it('should read directory contents recursively', async function() {
      let files = await Amma.reader(__dirname + '/fixtures/scan');
      expect(Object.keys(files).length).to.equal(6);
    });
  });
});
