"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
var ext_1 = require("../ext");
__export(require("./reader"));
var Amma = (function () {
    function Amma(options) {
        this._options = {
            recursive: true
        };
        if (!Amma._instance) {
            this.watch = new ext_1.Watcher();
            this._options = options || this._options;
            Amma._instance = this;
        }
    }
    Object.defineProperty(Amma.prototype, "emitter", {
        get: function () {
            return this.watch.expose();
        },
        enumerable: true,
        configurable: true
    });
    Amma.getInstance = function (options) {
        return this._instance || (this._instance = new this(options));
    };
    Amma.prototype.add = function (directory) {
        this.watch.watchDirectory(directory, this._options);
    };
    Amma.prototype.delete = function (directory) {
        this.watch.close(directory);
    };
    Amma.prototype.close = function () {
        this.watch.close();
    };
    return Amma;
}());
exports.default = Amma;
